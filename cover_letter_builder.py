import argparse
from datetime import date, datetime
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.lib.styles import getSampleStyleSheet
from jinja2 import Template

def main():
    my_info = {
        'name': 'Robert Darby Minter',
        'email': 'robertdminter@gmail.com',
        'cell': '+1 (240) 560-2905',
    }

    company_info = {
        'name': 'Company',
        'recruiter': 'Recruiter',
    }
    parser = argparse.ArgumentParser(prog='Cover Letter Builder')
    parser.add_argument('-c', '--company')
    parser.add_argument('-r', '--recruiter')
    args = parser.parse_args()

    if args.recruiter:
        company_info['recruiter'] = args.recruiter

    if args.company:
        company_info['name'] = args.company

    doc = SimpleDocTemplate('cover-letter.pdf', title=my_info['name'])
    document = []
    cover_letter = ''
    styles = getSampleStyleSheet()
    style = styles['Normal']
    with open('cover_letter.jinja2', encoding='utf-8') as file_:
        template = Template(file_.read())
        cover_letter = template.render(
            myself=my_info,
            company=company_info,
            date=date.isoformat(datetime.now())
        )
    for line in cover_letter.split('\n'):
        if not line:
            document.append(Spacer(0,16))
        p = Paragraph(line, style)
        document.append(p)
    doc.build(document)

if __name__ == '__main__':
    main()
